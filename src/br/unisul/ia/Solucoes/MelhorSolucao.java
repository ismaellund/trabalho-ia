package br.unisul.ia.Solucoes;

import java.util.ArrayList;
import java.util.List;

import br.unisul.ia.acoes.Acao;

public class MelhorSolucao {
	private List<Acao> melhorCarteira;
	private Double retornoCarteira;
	
	public MelhorSolucao(List<Acao> carteira, Double retornoCarteira) {
		this.melhorCarteira = cloneArrayAcoes(carteira);
		this.retornoCarteira = retornoCarteira;
	}
	
	public List<Acao> getCarteira() {
		return melhorCarteira;
	}
	public void setMelhorCarteira(List<Acao> melhorVizinho) {
		this.melhorCarteira = cloneArrayAcoes(melhorVizinho);
	}
	public Double getRetornoCarteira() {
		return retornoCarteira;
	}
	public void setRetornoCarteira(Double retornoCarteira) {
		this.retornoCarteira = retornoCarteira;
	}
	
	public void setSolucaoFinal(MelhorSolucao possivelMelhorSolucao) {
		if(possivelMelhorSolucao.getRetornoCarteira() > this.retornoCarteira) {
			this.melhorCarteira = possivelMelhorSolucao.getCarteira();
			this.retornoCarteira = possivelMelhorSolucao.getRetornoCarteira();
		}
	}
	
	private List<Acao> cloneArrayAcoes(List<Acao> acoesList) {
		List<Acao> newList = new ArrayList<Acao>();
		for(Acao acao : acoesList) {
			newList.add(acao.clone());
		}
		return newList;
		
	}

}
