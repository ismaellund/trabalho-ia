package br.unisul.ia.Solucoes;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import br.unisul.ia.acoes.Acao;

public class SolucaoFactory {

	private static SolucaoFactory instance = null;

	private SolucaoFactory() {
	}

	public static SolucaoFactory getInstance() {
		if (instance == null)
			instance = new SolucaoFactory();

		return instance;
	}

	@SuppressWarnings("unchecked")
	public List<List<Acao>> getTodasSimulacoes() {
		List<List<Acao>> solucoes = new ArrayList<List<Acao>>();
		Simulacoes simulacoes = new Simulacoes();
		Method[] metodos = Simulacoes.class.getDeclaredMethods();
		for (int i = 0; i < metodos.length; i++) {
			try {
				solucoes.add((List<Acao>) metodos[i].invoke(simulacoes));
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return solucoes;
	}

}
