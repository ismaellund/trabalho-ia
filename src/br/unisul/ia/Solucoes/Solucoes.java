package br.unisul.ia.Solucoes;

import java.util.ArrayList;
import java.util.List;
import br.unisul.ia.acoes.Acao;
import br.unisul.ia.acoes.AcaoA;
import br.unisul.ia.acoes.AcaoB;
import br.unisul.ia.acoes.AcaoC;
import br.unisul.ia.acoes.AcaoD;
import br.unisul.ia.acoes.AcaoE;

class Simulacoes {
	
	public List<Acao> getSimulacaoInicial() {
		List<Acao> acoes = new ArrayList<Acao>();
		acoes.add(new AcaoA(0.30));
		acoes.add(new AcaoB(0.25));
		acoes.add(new AcaoC(0.20));
		acoes.add(new AcaoD(0.15));
		acoes.add(new AcaoE(0.10));
		return acoes;
	}

	public List<Acao> getSimulacao1() {
		List<Acao> acoes = new ArrayList<Acao>();
		acoes.add(new AcaoA(0.35));
		acoes.add(new AcaoB(0.20));
		acoes.add(new AcaoC(0.20));
		acoes.add(new AcaoD(0.15));
		acoes.add(new AcaoE(0.10));
		return acoes;
	}

	public List<Acao> getSimulacao2() {
		List<Acao> acoes = new ArrayList<Acao>();
		acoes.add(new AcaoA(0.25));
		acoes.add(new AcaoB(0.30));
		acoes.add(new AcaoC(0.20));
		acoes.add(new AcaoD(0.15));
		acoes.add(new AcaoE(0.10));
		return acoes;
	}

	public List<Acao> getSimulacao3() {
		List<Acao> acoes = new ArrayList<Acao>();
		acoes.add(new AcaoA(0.25));
		acoes.add(new AcaoB(0.25));
		acoes.add(new AcaoC(0.25));
		acoes.add(new AcaoD(0.15));
		acoes.add(new AcaoE(0.10));
		return acoes;
	}

	public List<Acao> getSimulacao4() {
		List<Acao> acoes = new ArrayList<Acao>();
		acoes.add(new AcaoA(0.25));
		acoes.add(new AcaoB(0.25));
		acoes.add(new AcaoC(0.20));
		acoes.add(new AcaoD(0.20));
		acoes.add(new AcaoE(0.10));
		return acoes;
	}

	public List<Acao> getSimulacao5() {
		List<Acao> acoes = new ArrayList<Acao>();
		acoes.add(new AcaoA(0.25));
		acoes.add(new AcaoB(0.25));
		acoes.add(new AcaoC(0.20));
		acoes.add(new AcaoD(0.15));
		acoes.add(new AcaoE(0.15));
		return acoes;
	}
}
