package br.unisul.ia.acoes;

public class AcaoC implements Acao {
	
	private Double retornoAnual = 0.04128;
	private Double porcentagemAcao;
	
	public AcaoC(double porcentagemAcao) {
		this.porcentagemAcao = porcentagemAcao;
	}
	
	private AcaoC() {
		
	}
	
	public Double getRetornoAnual() {
		return retornoAnual;
	}
	public Double getPorcentagemAcao() {
		return porcentagemAcao;
	}
	public void setPorcentagemAcao(Double porcentagemAcao) {
		this.porcentagemAcao = porcentagemAcao;
	}
	
	public Acao clone() {
		AcaoC acao = new AcaoC();
		acao.porcentagemAcao = this.porcentagemAcao;
		acao.retornoAnual = this.retornoAnual;
		return acao;
	}
	
	@Override
	public String toString() {
		return String.format("%.0f%%",porcentagemAcao*100);
	}
}
