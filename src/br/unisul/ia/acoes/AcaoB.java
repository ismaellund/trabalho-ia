package br.unisul.ia.acoes;

public class AcaoB implements Acao {

	private Double retornoAnual = 0.25685;
	private Double porcentagemAcao;
	
	public AcaoB(double porcentagemAcao) {
		this.porcentagemAcao = porcentagemAcao;
	}
	
	private AcaoB() {
		
	}
	
	public Double getRetornoAnual() {
		return retornoAnual;
	}
	public Double getPorcentagemAcao() {
		return porcentagemAcao;
	}
	public void setPorcentagemAcao(Double porcentagemAcao) {
		this.porcentagemAcao = porcentagemAcao;
	}
	
	public Acao clone() {
		AcaoB acao = new AcaoB();
		acao.porcentagemAcao = this.porcentagemAcao;
		acao.retornoAnual = this.retornoAnual;
		return acao;
	}
	
	@Override
	public String toString() {
		return String.format("%.0f%%",porcentagemAcao*100);
	}

}
