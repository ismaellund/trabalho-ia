package br.unisul.ia.acoes;

public class AcaoD implements Acao {
	
	private Double retornoAnual = 0.05812;
	private Double porcentagemAcao;
	
	public AcaoD(double porcentagemAcao) {
		this.porcentagemAcao = porcentagemAcao;
	}
	
	private AcaoD() {
		
	}
	
	public Double getRetornoAnual() {
		return retornoAnual;
	}
	public Double getPorcentagemAcao() {
		return porcentagemAcao;
	}
	public void setPorcentagemAcao(Double porcentagemAcao) {
		this.porcentagemAcao = porcentagemAcao;
	}
	
	public Acao clone() {
		AcaoD acao = new AcaoD();
		acao.porcentagemAcao = this.porcentagemAcao;
		acao.retornoAnual = this.retornoAnual;
		return acao;
	}

	@Override
	public String toString() {
		return String.format("%.0f%%",porcentagemAcao*100);
	}

}
