package br.unisul.ia.acoes;

public class AcaoE implements Acao{

	private Double retornoAnual = 0.01731;
	private Double porcentagemAcao;
	
	public AcaoE(double porcentagemAcao) {
		this.porcentagemAcao = porcentagemAcao;
	}
	
	private AcaoE() {
		
	}
	
	public Double getRetornoAnual() {
		return retornoAnual;
	}
	public Double getPorcentagemAcao() {
		return porcentagemAcao;
	}
	public void setPorcentagemAcao(Double porcentagemAcao) {
		this.porcentagemAcao = porcentagemAcao;
	}
	
	public Acao clone() {
		AcaoE acao = new AcaoE();
		acao.porcentagemAcao = this.porcentagemAcao;
		acao.retornoAnual = this.retornoAnual;
		return acao;
	}

	@Override
	public String toString() {
		return String.format("%.0f%%",porcentagemAcao*100);
	}

}
