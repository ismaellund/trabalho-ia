package br.unisul.ia.acoes;

public class AcaoA implements Acao{
	
	private Double retornoAnual = 0.42292;
	private Double porcentagemAcao;
	
	public AcaoA(double porcentagemAcao) {
		this.porcentagemAcao = porcentagemAcao;
	}
	
	private AcaoA() {
		
	}
	
	public Double getRetornoAnual() {
		return retornoAnual;
	}
	public Double getPorcentagemAcao() {
		return porcentagemAcao;
	}
	public void setPorcentagemAcao(Double porcentagemAcao) {
		this.porcentagemAcao = porcentagemAcao;
	}
	
	public Acao clone() {
		AcaoA acao = new AcaoA();
		acao.porcentagemAcao = this.porcentagemAcao;
		acao.retornoAnual = this.retornoAnual;
		return acao;
	}
	
	@Override
	public String toString() {
		return String.format("%.0f%%",porcentagemAcao*100);
	}

}
