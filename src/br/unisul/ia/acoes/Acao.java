package br.unisul.ia.acoes;

public interface Acao extends Cloneable {
	
	public Double getRetornoAnual();
	
	public Double getPorcentagemAcao();
	
	public void setPorcentagemAcao(Double porcentagemAcao);
	
	public Acao clone();
}